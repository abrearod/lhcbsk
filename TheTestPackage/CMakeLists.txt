gaudi_subdir(TheTestPackage v0r0)

gaudi_install_python_modules()

gaudi_add_test(python
               COMMAND nosetests -v
                   ${CMAKE_CURRENT_SOURCE_DIR}/tests)
