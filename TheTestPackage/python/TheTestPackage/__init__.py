'''
Main file of TheTestPackage Python package.
'''

import os
import re

# import all submodules
for _path in __path__:
    for modname in os.listdir(_path):
        if modname.endswith('.py'): # we want only Python files
            modname = modname[:-3]
            if re.match(r'[a-zA-Z][a-zA-Z0-9_]', modname): # need valid module name
                exec 'import ' + modname
